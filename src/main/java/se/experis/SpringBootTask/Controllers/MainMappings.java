package se.experis.SpringBootTask.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainMappings {

    @GetMapping("/greeting")
    public String greetGuest(@RequestParam(name = "name", required = false, defaultValue = "") String name) {
        if(name.isEmpty())
        {
            return "Welcome to my page!";
        }

        return String.format("Welcome to my page, %s!", name);
    }

    @GetMapping("/reverse")
    public ResponseEntity reverseString(@RequestParam(name = "string", required = false, defaultValue = "") String userString) {
        if(userString.isEmpty()) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        StringBuilder stringBuilder = new StringBuilder(userString);
        return new ResponseEntity(stringBuilder.reverse().toString(), HttpStatus.OK);
    }
}
